const UserListsPage = require('../page/UserLists_po');
const page = new UserListsPage();

class UserListsActions {

/*     clickNextButton() {
        page.nextButton.waitForDisplayed(2000);
        page.nextButton.click();
    }
 */

    getNumberOfLists() {
        page.addNewListButton.waitForDisplayed(5000);
        return page.allMyLists.length;
    }

    addNewList() {
        page.addNewListButton.waitForDisplayed(2000);
        page.addNewListButton.click();
    }

    enterListName(value) {
        page.listNameInput.waitForDisplayed(5000);
        page.listNameInput.clearValue();
        page.listNameInput.setValue(value);
    }

    clickSaveButton() {
        page.saveButton.waitForDisplayed(2000);
        page.saveButton.click();
    }

    clickDeleteButton() {
        page.deleteButton.waitForDisplayed(2000);
        page.deleteButton.click();
    }

    clickUpdateButton() {
        page.updateButton.waitForDisplayed(2000);
        page.updateButton.click();
    }

}

module.exports = UserListsActions;
