const assert = require('assert');
const UserListsActions = require('./actions/UserLists_pa')
const MenuActions = require('./../Menu/actions/menu_pa');
const credentials = require('./../../credentials.json');
const help = require('../../helpers/helpers');
const validate = require('../../helpers/validators');
const wait = require('../../helpers/waiters');


const menuSteps = new MenuActions();
const pageSteps = new UserListsActions();


describe('User lists on the Hedonist Site', () => {
    
    beforeEach(() => {
        help.loginWithDefaultUser();
        wait.forSuccessNotification();
    });

    afterEach(() => {
        browser.reloadSession();
    });
    

    it('should create new list', () => {
        menuSteps.navigateToLists();
        wait.forSpinner();

        let numberOfListsBefore = pageSteps.getNumberOfLists();
        
        pageSteps.addNewList();
        pageSteps.enterListName(credentials.listName);
        pageSteps.clickSaveButton();
        
        let numberOfListsAfter = pageSteps.getNumberOfLists();

        assert.equal(numberOfListsAfter, (numberOfListsBefore + 1));
    });
        
    
    it('should delete existing list', () => {
        menuSteps.navigateToLists();
        wait.forSpinner();

        let numberOfListsBefore = pageSteps.getNumberOfLists();

        pageSteps.clickDeleteButton();
        pageSteps.clickDeleteButton();
        wait.forSpinner();

        let numberOfListsAfter = pageSteps.getNumberOfLists();

        assert.equal(numberOfListsAfter, (numberOfListsBefore - 1));
    });

    it('should edit existing list', () => {
        menuSteps.navigateToLists();
        wait.forSpinner();

        help.editItemInList(credentials.listName);
        pageSteps.enterListName(credentials.newListName);
        pageSteps.clickUpdateButton();

        validate.successNotificationTextIs(credentials.editListInfoSuccessText);
    });
}); 