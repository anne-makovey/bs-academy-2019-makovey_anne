class UserListsPage {

/*     get nameFieldLable () {return $('//label[contains(., "Name*")]')};
    get nextButton () {return $('div.tab-item:not([style="display: none;"]) span.is-success')};
 */
    get notification () {return $('div.toast div')};
    get addNewListButton () {return $('a[href="/my-lists/add"]')};
    get listNameInput () {return $('input#list-name')};
    get saveButton () {return $('//button[contains(., "Save")]')};
    get allMyLists () {return $$('div.container.place-item')};
    get deleteButton () {return $('//button[contains(., "Delete")]')};
    get updateButton () {return $('//button[contains(., "Update")]')};
};

module.exports = UserListsPage;

