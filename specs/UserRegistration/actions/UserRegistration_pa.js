const UserRegistrationPage = require('../page/UserRegistration_po');
const page = new UserRegistrationPage();

class UserRegistrationActions {

    enterFirstName(value) {
        page.firstNameInput.waitForDisplayed(2000);
        page.firstNameInput.clearValue();
        page.firstNameInput.setValue(value);
    }

    enterLastName(value) {
        page.lastNameInput.waitForDisplayed(2000);
        page.lastNameInput.clearValue();
        page.lastNameInput.setValue(value);
    }

    enterEmail(value) {
        page.emailInput.waitForDisplayed(2000);
        page.emailInput.clearValue();
        page.emailInput.setValue(value);
    }

    enterPassword(value) {
        page.passwordInput.waitForDisplayed(2000);
        page.passwordInput.clearValue();
        page.passwordInput.setValue(value);
    }

    clickCreateButton() {
        page.createButton.waitForDisplayed(2000);
        page.createButton.click();
    }

}

module.exports = UserRegistrationActions;
