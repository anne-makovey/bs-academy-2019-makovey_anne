const UserRegistrationActions = require('./actions/UserRegistration_pa')
const MenuActions = require('../Menu/actions/menu_pa');
const credentials = require('./../../credentials.json');
const help = require('../../helpers/helpers');
const validate = require('../../helpers/validators');


const menuSteps = new MenuActions();
const pageSteps = new UserRegistrationActions();


describe('User registration on the Hedonist site', () => {
    
    beforeEach(() => {
        browser.url(credentials.appUrl);
    });

    afterEach(() => {
        browser.reloadSession();
    });

    it('should register with valid data', () => {
        menuSteps.navigateToSignUp();

        pageSteps.enterFirstName(credentials.firstName);
        pageSteps.enterLastName(credentials.lastName);
        pageSteps.enterEmail(help.createRandomEmail());
        pageSteps.enterPassword(credentials.password);
        pageSteps.clickCreateButton();
        
        validate.successNotificationTextIs(credentials.registrationSuccessText);
        validate.pageTransition('login');
    });

}); 
