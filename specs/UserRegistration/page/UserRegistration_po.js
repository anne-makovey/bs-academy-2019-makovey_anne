class UserRegistrationPage {

    /* get nameFieldLable () {return $('//label[contains(., "Name*")]')};
    get nextButton () {return $('div.tab-item:not([style="display: none;"]) span.is-success')};
 */

    get firstNameInput () {return $('input[name=firstName]')};
    get lastNameInput () {return $('input[name=lastName]')};
    get emailInput () {return $('input[name="email"]')};
    get passwordInput () {return $('input[type="password"]')};
    get createButton () {return $('button.is-primary')};

};

module.exports = UserRegistrationPage;

