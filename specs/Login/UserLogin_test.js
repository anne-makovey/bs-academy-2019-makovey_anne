const credentials = require('./../../credentials.json');
const help = require('../../helpers/helpers');
const validate = require('../../helpers/validators');



describe('Login on the Hedonist site', () => {

    afterEach(() => {
        browser.reloadSession();
    });


    it('should not login with wrong credentials', () => {
        help.loginWithCustomUser(credentials.wrongEmail, credentials.wrongPassword);
        validate.errorNotificationTextIs(credentials.incorrectCredentialsErrorText);
    });
}); 
