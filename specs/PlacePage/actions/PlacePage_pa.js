const PlacePage = require('../page/PlacePage_po');
const page = new PlacePage();

class PlaceActions {

    enterReviewText(value) {
        page.reviewInput.waitForDisplayed(4000);
        page.reviewInput.clearValue();
        page.reviewInput.setValue(value);
    }

    clickPostButton() {
        page.postButton.waitForDisplayed(5000);
        page.postButton.click();
    }

    getAddedReviewText() {
        page.addedReviewText.waitForDisplayed(10000);
        return page.addedReviewText.getText();
    }

    clickRateButton() {
        page.rateButton.waitForDisplayed(5000);
        page.rateButton.click();
    }

    selectRate(rate) {
        page.itemsRateModal[rate].waitForDisplayed(5000);
        page.itemsRateModal[rate].click();
        page.itemsRateModal[rate].waitForDisplayed(5000, true);
    }

}

module.exports = PlaceActions;
