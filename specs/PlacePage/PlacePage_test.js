const assert = require('assert');
const PlaceActions = require('./actions/PlacePage_pa');
const PlaceObjects = require('./page/PlacePage_po');
const credentials = require('./../../credentials.json');
const help = require('../../helpers/helpers');
const validate = require('../../helpers/validators');
const wait = require('../../helpers/waiters');

const pageSteps = new PlaceActions();
const pageObjects = new PlaceObjects();


describe('Places on the Hedonist site', () => {
    
    beforeEach(() => {
        help.loginWithDefaultUser();
        wait.forSuccessNotification();
        help.clickItemInList('place');
        wait.forSpinner();

    });

    afterEach(() => {
        browser.reloadSession();
    });

    it('should add new review', () => {
        pageSteps.enterReviewText(credentials.reviewText);
        pageSteps.clickPostButton();

        validate.successNotificationTextIs(credentials.addReviewSuccessText);
        assert.equal(pageSteps.getAddedReviewText(), credentials.reviewText);
    });

    it('should rate place', () => {
        pageSteps.clickRateButton();
        pageSteps.selectRate(3);
        wait.forSuccessNotification();

        pageSteps.clickRateButton();
        validate.elementGetClass(pageObjects.itemsRateModal[3], 'chosenRating');
    });


}); 

