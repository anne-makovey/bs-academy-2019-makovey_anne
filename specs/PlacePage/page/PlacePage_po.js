class PlacePage {

    get reviewInput () {return $('.add-review textarea')};
    get postButton () {return $('//button[contains(., "Post")]')};
    get addedReviewText () {return $('.reviews-section-list p')};

    get rateButton () {return $('button.rating')};
    get itemsRateModal () {return $$('div.smileys__item i')}
};

module.exports = PlacePage;

