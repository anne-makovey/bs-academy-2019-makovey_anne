class NewPlacePage {

/*     get nameFieldLable () {return $('//label[contains(., "Name*")]')};
    get nextButton () {return $('div.tab-item:not([style="display: none;"]) span.is-success')};
 */

    get namePlaceInput () {return $('//label[contains(., "Name")]/../following-sibling::div/div/div/input')};
    get cityInput () {return $('div.place-location input')};
    get zipInput () {return $('//label[contains(., "Zip")]/../following-sibling::div/div/div/input')};
    get addressInput () {return $('//label[contains(., "Address")]/../../div/div/div/input')};
    get PhoneInput () {return $('input[type=tel]')};
    get siteInput () {return $('//label[contains(., "Website")]/../../div/div/div/input')};
    get descriptionInput () {return $('//label[contains(., "Description")]/../../div/div/div/textarea')};

    get imageInput () {return $('input[type=file]')};
    get imagePreview () {return $('img.image-preview')};

    get nextButton () {return $('//div[not(contains(@style,"display: none"))]/div/span[contains(., "Next")]')};
    get addPlaceButton () {return $('//span[contains(@class, "button") and contains(., "Add")]')};

    get categorySelect () {return $('//option[contains(., "category")]/..')};
    get tagsSelect () {return $('//option[contains(., "tags")]/..')};
    get allTagsOptions () {return $$('//option[contains(., "tags")]/../option')};

    get switchFeatures () {return $('.switch .check')};

    get placeName () {return $('.place-venue__place-name')};
};

module.exports = NewPlacePage;

