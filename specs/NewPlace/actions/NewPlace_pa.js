const NewPlacePage = require('../page/NewPlace_po');
const page = new NewPlacePage();
const path = require('path');

class NewPlaceActions {


    enterPlaceName(value) {
        page.namePlaceInput.waitForDisplayed(2000);
        page.namePlaceInput.clearValue();
        page.namePlaceInput.setValue(value);
    }

    enterCity(value) {
        page.cityInput.waitForDisplayed(2000);
        page.cityInput.clearValue();
        page.cityInput.setValue(value);
    }

    enterZip(value) {
        page.zipInput.waitForDisplayed(2000);
        page.zipInput.clearValue();
        page.zipInput.setValue(value);
    }

    enterAddress(value) {
        page.addressInput.waitForDisplayed(2000);
        page.addressInput.clearValue();
        page.addressInput.setValue(value);
    }

    enterPhone(value) {
        page.PhoneInput.waitForDisplayed(2000);
        page.PhoneInput.clearValue();
        page.PhoneInput.setValue(value);
    }

    enterSite(value) {
        page.siteInput.waitForDisplayed(2000);
        page.siteInput.clearValue();
        page.siteInput.setValue(value);
    }

    enterDescription(value) {
        page.descriptionInput.waitForDisplayed(2000);
        page.descriptionInput.clearValue();
        page.descriptionInput.setValue(value);
    }

    clickNextButton() {
        page.nextButton.waitForDisplayed(5000);
        page.nextButton.click();
        page.nextButton.waitForDisplayed(5000, true);
    }

    uploadImage(imagePath) {
        page.imageInput.waitForExist(5000);
        page.imageInput.setValue(path.resolve(imagePath));
    }

    selectCategory(index) {
        page.categorySelect.waitForDisplayed(2000);
        page.categorySelect.selectByIndex(index);
    }

    selectTag(index) {
        browser.waitUntil(() => {
            return (page.allTagsOptions.length > 1)}, 3000);
        page.tagsSelect.selectByIndex(index);
    }

    toggleSwitch() {
        page.switchFeatures.waitForDisplayed(2000);
        page.switchFeatures.click();
    }

    getPlaceName() {
        page.placeName.waitForDisplayed(10000);
        return page.placeName.getText();
    }

    clickAddPlaceButton() {
        page.addPlaceButton.waitForDisplayed(5000);
        page.addPlaceButton.click();
        page.addPlaceButton.waitForDisplayed(5000, true);
    }

}

module.exports = NewPlaceActions;
