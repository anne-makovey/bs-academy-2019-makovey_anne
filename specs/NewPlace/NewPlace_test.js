const assert = require('assert');
const NewPlaceActions = require('./actions/NewPlace_pa')
const MenuActions = require('../Menu/actions/menu_pa');
const credentials = require('./../../credentials.json');
const help = require('../../helpers/helpers');
const wait = require('../../helpers/waiters');


const menuSteps = new MenuActions();
const pageSteps = new NewPlaceActions();


describe('Places on the Hedonist site', () => {
    
    beforeEach(() => {
        help.loginWithDefaultUser();
        wait.forSuccessNotification();

    });

    afterEach(() => {
        browser.reloadSession();
    });

    it('should create new place with valid data', () => {
        menuSteps.navigateToNewPlace();

        //General tab
        pageSteps.enterPlaceName(credentials.placeName);
        pageSteps.enterCity(credentials.city);
        pageSteps.enterZip(credentials.zipCode);
        pageSteps.enterAddress(credentials.address);
        pageSteps.enterPhone(credentials.phone);
        pageSteps.enterSite(credentials.site);
        pageSteps.enterDescription(credentials.description);
        pageSteps.clickNextButton();
        
        //Photos tab
        pageSteps.uploadImage(credentials.placeImage);
        pageSteps.clickNextButton();

        //Location tab
        pageSteps.clickNextButton();

        //Categories tab
        pageSteps.selectCategory(2);
        pageSteps.selectTag(2);
        pageSteps.clickNextButton();

        //Features tab
        pageSteps.toggleSwitch();
        pageSteps.clickNextButton();

        //Hours tab
        pageSteps.clickNextButton();
        //Add place tab
        pageSteps.clickAddPlaceButton();

        assert.equal(pageSteps.getPlaceName(), credentials.placeName);
        
    });
}); 

